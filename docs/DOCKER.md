# Pidgin Docker Test Environment Documentation
This file provides a step-by-step guide to installing, setting up
and using the AmadorUAVs Docker test environment for Pidgin.

NOTE: The Docker test environment is currently only supported
on Debian, Fedora and Arch-Linux based Linux distributions. If
you don't have a Linux machine, log into Saturn with the
credentials given to you.

## Installing ##
1. Clone this repo and `cd` into it.
2. Run the `setup.sh` script with `./setup.sh`.

## Setting up ##
1. `cd` to the `docker/pidgin` directory.
2. If you have an NVIDIA GPU, you may need to
install the correct NVIDIA driver version for
the Docker images. To do this, use the `nvidia-finetune.sh` script
located within the `common/nvidia` directory.
3. That's it!

## Running ##
1. Go back to the `docker/pidgin` directory.
2. Disable X access control with `xhost +`.
3. If you have an NVIDIA GPU, run `docker-compose -f docker-compose.nvidia.yml up`
to start the testing environment. Otherwise, run `docker-compose -f docker-compose.mesa.yml up`.
4. Attach to the client container using `docker exec -it pidgin_client_1 /bin/bash`.

Any questions about the docker test environment can be directed to
the #software channel on the AmadorUAVs Discord, vwangsf@gmail.com or coder.kalyan@gmail.com.
