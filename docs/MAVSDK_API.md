# Using the MAVSDK API #
Pidgin has an in-house API for accessing MAVSDK and PX4 functions.
Here's how to use it:

## The Drone class ##
Everything in the API is done by the Drone class (waypoint adding, telemetry, etc.)
To create a Drone object:

1. Use `from mavsdk_bindings import Drone` to import the Drone class
2. Create a new drone object using `drone = Drone(url)`.
URL in this case is one of these:

| Connection | URL Format                 | Description                                                                                                                  | 
|------------|----------------------------|------------------------------------------------------------------------------------------------------------------------------|
| TCP        | `tcp://host:bind_port`     | Generally not used. Host is the PX4 server (`gazebo` in the Docker env) and `bind_port` is the PX4 port (standard is 14550). | 
| UDP        | `udp://host:bind_port`     | Used for simulations. Host is usually `gazebo` in the docker environment. `bind_port` is usually 14550.                      | 
| Serial     | `serial://devnode:baudrate`| Used for radio/non-wifi connections. devnode will usually be /dev/ttyAMA0 on Unix systems and baudrate is generally 57600.   | 

3. You're ready!

## Takeoff and Landing ##
To fly the drone, first you have to *takeoff*. To do this, use the `drone.takeoff()` function.
(The `takeoff()` function auto-arms the drone.)

To land the drone, use `drone.return_to_land()`. This will return the drone to its starting position.

## Waypoints ##
1. In order to add waypoints to the drone, first set a new *mission*. To do this, use `drone.new_mission()`
to clear the current mission (None in this case) and make a new one.
2. Create a new Waypoint object using `waypoint = Waypoint()`.
  a. Set the waypoint latitude with `waypoint.set_latitude(LATITUDE_DEGREES)`.
  b. Set the waypoint latitude with `waypoint.set_longitude(LATITUDE_DEGREES)`.
  c. Set the waypoint latitude with `waypoint.set_height(HEIGHT_METERS)`. Height is relative to ground, not MSL - in the case of the competition grounds this is MSL - 3.99288.
3. Add this waypoint to the mission using `drone.add_waypoint(waypoint)`.
4. Optionally, print out a mission summary using `drone.print_mission()`. This
will print out all waypoints/actions in the mission.
5. Arm the drone using `drone.arm()`. If the drone has already taken off, skip
this step.
6. Start the mission using `drone.start_mission()`.

## Telemetry ##
The API is capable of accessing the Drone's current position and its battery level.

`drone.get_position()` -> returns a `Position` object. Use `position.latitude()`, `position.longitude()`, and `position.height()` to access its attributes.
`drone.battery` -> returns the remaining estimated battery percentage, as a decimal.
