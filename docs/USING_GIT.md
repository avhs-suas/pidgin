# Using Git: A Basic Guide #
This file describes the basics of Git that
you need to know to modify code on the repository.

## Installation ##
NOTE: We VERY HIGHLY RECOMMEND you use a Linux system
to develop on the AmadorUAVs repos. MacOS is untested for support.
Windows will not work at all unless you use WSL or Cygwin, and
even then support is untested. It's super easy to dual boot Ubuntu
now; just search up an online tutorial, we promise it'll be worth it.

### Windows ###
1. Download the latest Git from https://git-scm.com/.
2. Open the installer.
3. Follow the prompts. Select all defaults.
4. Finish installation.
5. To use Git, go to the Start Menu and search for "Git Bash".
6. You're ready!

### MacOS ###
1. Download the installer through https://git-scm.com/
2. Open it and follow the prompts. Select all defaults.
3. Open Terminal.
4. Run `git --version` to check that you have Git. If it
says something like `git version 2.9.1` then it installed correctly.
5. You're ready!

### Linux ###
1. Install `git` through your package manager (eg. `pacman -S git`)
2. You're ready!


## Terminology ##
- Terminal: Git Bash on Windows, Terminal on MacOS and Linux.
- Local: the local copy of the code on your computer.
- Remote: the Gitlab repo hosted in the cloud.
- Clone: get a local copy of the code from the Gitlab repo.
- Branch: a "track" of code that you develop in. This is where
all your commits go.
- Commit: Add a new Git commit (log entry saying
what you did). In the context of commands, this usually
means `git add . && git commit -m "message"`
- Push: push code from local to remote.
`git push`
- Pull: Update your local copy of the code from remote.
`git pull`
- Merge: Combine two branches.
`git merge branch1 branch2` (where branch1 will merge into branch2)
- Fix Merge Errors: Occasionally, a merge will fail because
of conflicting edits to the code. Fix this with `git mergetool`.

## Getting the Code ##
1. Find the repository you want to clone through https://gitlab.com/avhs-suas.
2. Click on the Clone button in the top right corner and copy the HTTPS link.
3. Go to your terminal and paste it: `git clone [link]`
4. Enter your Gitlab user and password.
5. You should have a folder in your home directory containing the source code.

## Using Branches ##
1. To list all branches, use `git branch -a`.
2. To switch to a branch, use `git checkout [branch-name]`.
3. To create a new branch, use `git checkout -b [branch-name]`.

## Modifying the Code ##
1. Change some code. If you've already changed some code, skip this step.
2. Make sure you're at the root of the repo (the folder you originally cloned to).
3. Run `git add .` to add all your changes.
4. Run `git commit -m "short description of changes"` to commit. Make your short
description easy to read and sensible.
5. Run `git pull` (in case remote code was modified).
6. Run `git push`.

## Merging ##
- Merging `develop` into your branch: In accordance with our Continuous Integration guidelines,
you should merge in code from `develop` at least once a day to avoid merge errors. To
do this, run `git merge origin/develop`.
- Merging your branch into `develop` (or any other types of merge):
The CI script handles this. Please don't try and
do this yourself. Chances are that if you actually need to do this,
you should grab a sysadmin and/or somebody who can merge it manually and fix whatever
breaks.

### Handling Merge Errors ###
At some point, when merging or pulling, you will run into a merge error.
This is caused by code changes in your local copy conflicting with code
changes in the remote copy. To fix these errors:

1. Get a good merge tool. I highly recommend [Meld](http://meldmerge.org/);
it has an intuitive interface that makes merging 10x easier.
2. Go to the terminal and run `git mergetool`.
3. Fix the errors; in Meld, when there is an error, there will be a blob of
highlighted code on the left side and a blob on the right side. The left side
is usually LOCAL and the right side is usually REMOTE. Click the arrows to use
your code or their code respectively. You can also manually merge code by typing it in.
4. Save.
5. Run `git add . && git commit -m "Fixed merge error"`.
6. `git push` so other people don't have to deal with more merge errors.

If you have any questions about using Git, the Discord Software channel
is always open to questions and you can always send me an email at vwangsf@gmail.com.
