# This handy script will install deps for Debian, Fedora and Arch Linux
# based systems.
# NOTE: DO NOT SWITCH APT-GET TO APT. This script uses
# apt-get for backward compat reasons; don't change unless
# you ABSOLUTELY ARE SURE NOBODY USES APT-GET ANYMORE.
# TODO: Switch Debian/Ubuntu from outdated system repos to official
# Docker repo.

# Perhaps the if then statements are stupid, but I really don't
# know a better way to do this.
# Oh, and if a bash dev is reading this, can your language PLEASE
# GET SOME NORMAL IF STATEMENTS THAT MAKE SENSE THANK YOU.
# - Vincent Wang, 2019-06-12

if [ -f /usr/bin/pacman ];
then
    # We're on an Arch based system, use pacman to install
    # Update full system first
    echo "Arch Linux-based system detected, using pacman..."
    echo "Updating system..."
    sudo pacman -Syu

    # get pip (python preinstalled)
    echo "Installing pip..."
    sudo pacman -S python-pip

    # Install Docker and Docker Compose
    echo "Installing Docker and Docker Compose..."
    sudo pacman -S docker docker-compose

    echo "Done installing deps! Have a nice day :)"
elif [ -f /usr/bin/apt-get ];
then
    # We're on a Debian based system, use apt-get to install
    # Update full system first
    echo "Debian-based system detected, using apt-get..."
    echo "Updating system..."
    sudo apt update && sudo apt upgrade -y

    # get python3 (just in case) and pip
    echo "Installing pip..."
    sudo apt install python3 python3-pip

    # Install Docker and Docker Compose (using system repo, may change later)
    echo "Installing Docker and Docker Compose..."
    sudo apt install docker
    sudo -H pip3 install docker-compose

    echo "Done installing deps! Have a nice day :)"
elif [ -f /usr/bin/dnf ];
then
    # We're on a Fedora based system, use dnf to install
    # Update full system first
    echo "Fedora-based system detected, using dnf..."
    echo "Updating system..."
    sudo dnf upgrade --refresh

    # Get python3 (just in case) and pip
    echo "Installing pip..."
    sudo dnf install python3 python3-pip

    # Remove old Docker versions
    echo "Removing old Docker versions..."
    sudo dnf remove docker \
                  docker-client \
                  docker-client-latest \
                  docker-common \
                  docker-latest \
                  docker-latest-logrotate \
                  docker-logrotate \
                  docker-selinux \
                  docker-engine-selinux \
                  docker-engine

    echo "Automagically installing latest Docker..."
    sudo dnf -y install dnf-plugins-core
    sudo dnf config-manager \
        --add-repo \
        https://download.docker.com/linux/fedora/docker-ce.repo

    sudo dnf install docker-ce docker-ce-cli containerd.io

    # Get Docker Compose
    sudo -H pip3 install docker-compose

    echo "Done installing deps! Have a nice day :)"
else
    echo "Sorry, your package manager or OS isn't supported. :/"
    echo "If you have an older Debian-based system, try installing aptitude."
fi

# Login to gitlab registry
sudo docker login registry.gitlab.com/avhs-suas/pidgin
