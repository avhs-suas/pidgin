/* This file contains the header declaration for
 * the main Drone class.
 * 
 * Most methods will return 0 for success, 1 otherwise.
 * Some may return a data type (see data.h).  
 * 
 * Possible Point of Confusion: mission is the mission
 * runner pointer, whereas mission_items is the actual
 * list of waypoints. */
#ifndef __DRONE_H
#define __DRONE_H

#include <mavsdk/mavsdk.h>
#include <mavsdk/plugins/action/action.h>
#include <mavsdk/plugins/telemetry/telemetry.h>
#include <mavsdk/plugins/mission/mission.h>
#include <string>
#include <vector>
#include "data.h"
using namespace std;
namespace dc = mavsdk;

class Drone
{
	private:
		dc::Mavsdk drone;
		shared_ptr<dc::Action> action;
		shared_ptr<dc::Telemetry> telemetry;
		shared_ptr<dc::Mission> mission;
		vector<shared_ptr<dc::MissionItem>> mission_items;

		Position position;
		float battery_percentage;

		void wait_for_calibration();
	public:
		// Constructor
		Drone(string url);
		
		// Mission functions
		int print_mission();
		int start_mission();
		int new_mission();

		// Actions
		int takeoff(float takeoff_height_m = 200);
		int return_to_launch();

		int add_waypoint(Waypoint waypoint);
		int remove_waypoint(Waypoint waypoint);

		// Telemetry
		float battery();
		Position get_position();
		
};

#endif //__DRONE_H
