#!/usr/bin/env python

"""
setup.py file for SWIG example
"""

from distutils.core import setup, Extension


mavsdk_bindings = Extension('_mavsdk_bindings',
                            sources=['mavsdk_bindings_wrap.cxx', 'drone.cpp'],
                            include_dirs=["/usr/local/include/mavsdk"],
                            libraries=["mavsdk_action", "mavsdk_telemetry", "mavsdk_mission", "mavsdk"])

setup (name = 'mavsdk_bindings',
       version = '0.1',
       author      = "AmadorUAVs",
       description = """A simple library of bindings for MAVSDK for use in AmadorUAVs internal projects""",
       ext_modules = [mavsdk_bindings],
       py_modules = ["mavsdk_bindings"],
       )
