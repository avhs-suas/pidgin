/* Various data types for making Python <-> C++
 * communication easier. Many of these are simple
 * classes containing a few attributes. 
 *
 * NOTE TO THAT ONE GUY: Classes and structs compile
 * to the EXACT SAME CODE. If you want to switch
 * everything to structs thinking it'll make the code
 * faster, IT WILL NOT. These are classes and not structs
 * simply because I think it's more consistent and readable.
 * If you want to switch to structs for readability, be my
 * guest, but IT WILL NOT IMPROVE PERFORMANCE. */
#ifndef __DATA_H
#define __DATA_H
#include <string>

class Position {
	private:
		double lat;
		double lon;
		float height_m;
	public:
		Position() { }

		double latitude() {
			return lat;
		}

		double longitude() {
			return lon;
		}

		float height() {
			return height_m;
		}


		void set_latitude(double lat) {
			this->lat = lat;
		}

		void set_longitude(double lon) {
			this->lon = lon;
		}

		void set_height(float height_m) {
			this->height_m = height_m;
		}

};

class Waypoint: public Position { };

#endif //__DATA_H
