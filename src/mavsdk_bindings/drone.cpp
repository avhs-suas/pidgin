/* This file contains the implementation for the
 * main Drone class for the MAVSDK API. 
 *
 * NOTE: For simulation, the drone will take off
 * even if it requires calibration. For safety, 
 * uncomment line 100. 
 */
#include <mavsdk/mavsdk.h>
#include <mavsdk/plugins/action/action.h>
#include <mavsdk/plugins/telemetry/telemetry.h>
#include <mavsdk/plugins/mission/mission.h>
#include <string>
#include <vector>
#include <iostream>
#include <chrono>
#include <thread>
#include <future>
#include <stdlib.h>
#include "drone.h"
#include "data.h"
using namespace std;
using namespace std::chrono;
using namespace std::this_thread;
namespace dc = mavsdk;

// Below are the implementations for the functions of the main Drone class

Drone::Drone(string conn_url) 
{

	dc::ConnectionResult conn_result;

	conn_result = drone.add_any_connection(conn_url);
	// Wait for the system to connect via heartbeat
	while (!drone.is_connected()) {
		sleep_for(seconds(1));
	}

	if (conn_result == dc::ConnectionResult::SUCCESS) {
		cout << "Connect success!" << endl;
		// Create pointer to Action plugin
		dc::System &system = drone.system();
		action = make_shared<dc::Action>(system);
		mission = make_shared<dc::Mission>(system);
		telemetry = make_shared<dc::Telemetry>(system);
		telemetry->set_rate_position(1.0);
		telemetry->set_rate_battery(1.0);
		
		// Define telemetry update callbacks
		telemetry->position_async([this](dc::Telemetry::Position pos) {
			position.set_latitude(pos.latitude_deg);
			position.set_longitude(pos.longitude_deg);
			position.set_height(pos.relative_altitude_m);
		});

		telemetry->battery_async([this](dc::Telemetry::Battery batt) {
			battery_percentage = batt.remaining_percent;
		});

	} else {
		cout << "Connect Fail :(" << endl;
		exit (EXIT_FAILURE);
	}
}

// Mission functions
int Drone::print_mission()
{
	cout << "Current Mission Summary: " << endl;
	if (mission_items.empty()) {
		cout << "	No actions/waypoints added." << endl;
	}
	for (auto it = mission_items.begin(); it != mission_items.end(); it++) {
		//Construct a waypoint representation
		string waypoint_text = "Waypoint(LAT ";
		waypoint_text += to_string((*it)->get_latitude_deg());
		waypoint_text += " LONG ";
		waypoint_text += to_string((*it)->get_longitude_deg());
		waypoint_text += " HEIGHT ";
		waypoint_text += to_string((*it)->get_relative_altitude_m());
		waypoint_text += "m)";

		cout << "	" << waypoint_text << endl;
	}
	return 0;
}

int Drone::start_mission()
{
	// Upload the mission (get result via promise)
	auto prom = make_shared<promise<dc::Mission::Result>>();
	auto future_result = prom->get_future();
	mission->upload_mission_async(
	mission_items, [prom](dc::Mission::Result result) {
		prom->set_value(result);
	});

	const dc::Mission::Result result = future_result.get();
	if (result != dc::Mission::Result::SUCCESS) {
		std::cout << "Mission upload failed (" << dc::Mission::result_str(result) << "), exiting." << std::endl;
		return 1;
	}

	std::cout << "Mission uploaded." << std::endl;

	// Start mission (get result via promises)
	prom = make_shared<promise<dc::Mission::Result>>();
	future_result = prom->get_future();
	mission->start_mission_async(
	[prom](dc::Mission::Result result) {
		prom->set_value(result);
	});
	
	const dc::Mission::Result mission_start_result = future_result.get(); //Wait on result
	if (mission_start_result != dc::Mission::Result::SUCCESS) {
		std::cout << "Mission start failed (" << dc::Mission::result_str(mission_start_result) << "), exiting." << std::endl;
		return 1;
	}
	std::cout << "Started mission." << std::endl;

	return 0;
}

int Drone::new_mission()
{
	mission_items.clear();
	return 0;
}

void Drone::wait_for_calibration()
{
	dc::Telemetry::Health check_health = telemetry->health();
	bool calibration_required = false;
	if (!check_health.gyrometer_calibration_ok) {
		cout << "Gyro requires calibration." << endl;
		calibration_required=true;
	}
	if (!check_health.accelerometer_calibration_ok) {
		cout << "Accelerometer requires calibration." << endl;
		calibration_required=true;
	}
	if (!check_health.magnetometer_calibration_ok) {
		cout << "Magnetometer (compass) requires calibration." << endl;
		calibration_required=true;
	}
	if (!check_health.level_calibration_ok) {
		cout << "Level calibration required." << endl;
		calibration_required=true;
	}
	if (calibration_required) {
		// exit(1);
	}
	
	
	// Check if ready to arm (reporting status)
	while (telemetry->health_all_ok() != true) {
		cout << "Vehicle not ready to arm. Waiting on:" << endl;
		dc::Telemetry::Health current_health = telemetry->health();
		if (!current_health.global_position_ok) {
			cout << "  - GPS fix." << endl;
		}
		if (!current_health.local_position_ok) {
			cout << "  - Local position estimate." << endl;
		}
		if (!current_health.home_position_ok) {
			cout << "  - Home position to be set." << endl;
		}
		sleep_for(seconds(1));
	}
}

// Waypoint Control
int Drone::add_waypoint(Waypoint waypoint)
{
	shared_ptr<dc::MissionItem> point(new dc::MissionItem());
	point->set_position(waypoint.latitude(), waypoint.longitude());
	point->set_relative_altitude(waypoint.height());

	mission_items.push_back(point); 
	return 0;
}

int Drone::remove_waypoint(Waypoint waypoint)
{

	return 0;
}

// Takeoff and Land
int Drone::takeoff(float takeoff_height_m)
{
	wait_for_calibration();
	cout << "Ready - Arming!" << endl;
	action->arm();

	action->set_takeoff_altitude(takeoff_height_m);
	action->takeoff();
	cout << "Took off!" << endl;

	return 0;
}

int Drone::return_to_launch()
{
	action->return_to_launch();
	return 0;	
}


// Telemetry polling functions
Position Drone::get_position()
{
	return position;
}

float Drone::battery()
{
	return battery_percentage;
}

// Internal functions
