# This script fixes version mismatches in the common:nvidia

# Download the right NVIDIA driver
VERSION=$(nvidia-smi -q | grep "Driver Version" | awk '{print $4}')
wget http://us.download.nvidia.com/XFree86/Linux-x86_64/${VERSION}/NVIDIA-Linux-x86_64-${VERSION}.run -O nvidia/NVIDIA-DRIVER.run

for IMAGE in registry.gitlab.com/amadoruavs/pidgin/common:nvidia registry.gitlab.com/amadoruavs/pidgin/gazebo:nvidia
do
    sudo docker stop nvidia-finetune-tmp
    sudo docker rm nvidia-finetune-tmp
    # Make a new temp container
    echo "Configuring $IMAGE..."
    sudo docker run -it -d --name nvidia-finetune-tmp -v "NVIDIA-DRIVER.run:/tmp/NVIDIA-DRIVER.run" $IMAGE
    sudo docker start nvidia-finetune-tmp
    
    # Uninstall old driver library
    echo "Uninstalling old drivers..."
    sudo docker exec -it nvidia-finetune-tmp sh /tmp/NVIDIA-DRIVER.run --uninstall --skip-module-unload
    
    # Install new
    echo "Installing new drivers..."
    sudo docker exec -it nvidia-finetune-tmp sh /tmp/NVIDIA-DRIVER.run -a -N --ui=none --no-kernel-module
    
    # Commit changes
    echo "Committing changes to $IMAGE..."
    sudo docker commit nvidia-finetune-tmp $IMAGE
    
    # Remove temp container
    sudo docker stop nvidia-finetune-tmp
    sudo docker rm nvidia-finetune-tmp
done
