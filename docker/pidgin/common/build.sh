# Builds, tags and (optionally) pushes common images

# Automatically exit on any command failure
set -e

# idiomatic parameter and option handling in sh
# Copied from SuperUser/StackExchange
TOBUILD="DEFAULT"
PUSH="FALSE"

# Define the help menu manually because apparently you need to in bash
help() {
    echo "Usage: ./build.sh [--all] [--push] [--nogui] [--help]"
    echo
    echo "  --all       Build all Docker common images (not recommended"
    echo "  --push      Push images to registry once built"
    echo "  --base      Only build the base version (builds GUI image + base by default)"
    echo "  --help      Print this help"
}

while test $# -gt 0
do
    case "$1" in
        --all) echo "Building all Docker common images."
               TOBUILD="ALL"
            ;;
        --push) echo "Pushing Docker images to registry when done."
               PUSH="TRUE"
            ;;
        --base) echo "Building base image only."
                if [ $TOBUILD == "ALL" ]; then
                    echo "WARN: Overriding previous --all argument."
                fi
                TOBUILD="BASE"
            ;;
        --help) help; exit 0
            ;;
        *) echo "WARN: Unrecognized argument $1, ignoring"
            ;;
    esac
    shift
done

# Build base image bc we need it no matter which driver
sudo docker build --no-cache -t registry.gitlab.com/amadoruavs/pidgin/common:base base
if [ "$PUSH" == "TRUE" ]; then
    sudo docker push registry.gitlab.com/amadoruavs/pidgin/common:base
fi

# Stop here if user selected --base
if [ "$TOBUILD" == "BASE" ]; then
    exit 0
fi

# Check if we're building ALL; if not, autodetect driver

if [ "$TOBUILD" != "ALL" ]; then
    # Check whether we're using NVIDIA
    if command -v nvidia-smi >/dev/null 2>&1; then
        # We're using NVIDIA driver
        # NOTE: This script doesn't care what your host system's
        # NVIDIA version is. Run nvidia-finetune.sh to fix version
        # mismatches.
        echo "Using NVIDIA proprietary driver"

        # Clean old version
        rm nvidia/NVIDIA-DRIVER.run

        # Download driver
        VERSION=430.26
        wget http://us.download.nvidia.com/XFree86/Linux-x86_64/${VERSION}/NVIDIA-Linux-x86_64-${VERSION}.run -O nvidia/NVIDIA-DRIVER.run

        # Build Docker image
        sudo docker build --no-cache -t registry.gitlab.com/amadoruavs/pidgin/common:nvidia nvidia
        if [ "$PUSH" == "TRUE" ]; then
            # Push
            sudo docker push registry.gitlab.com/amadoruavs/pidgin/common:nvidia
        fi
    else
        echo "Using open source MESA driver"
        # We're not using nvidia, it's probably safe to use MESA
        sudo docker build --no-cache -t registry.gitlab.com/amadoruavs/pidgin/common:mesa mesa/

        if [ "$PUSH" == "TRUE" ]; then
            # Push mesa image
            sudo docker push registry.gitlab.com/amadoruavs/pidgin/common:mesa
        fi
    fi
else
    # Build all
    # Use automagic to get NVIDIA driver version and download right version
    VERSION=$(nvidia-smi -q | grep "Driver Version" | awk '{print $4}')
    wget http://us.download.nvidia.com/XFree86/Linux-x86_64/${VERSION}/NVIDIA-Linux-x86_64-${VERSION}.run -O nvidia/NVIDIA-DRIVER.run
    sudo docker build --no-cache -t registry.gitlab.com/amadoruavs/pidgin/common:nvidia nvidia
    sudo docker build --no-cache -t registry.gitlab.com/amadoruavs/pidgin/common:mesa mesa

    if [ "$PUSH" == "TRUE" ]; then
        # Push all
        sudo docker push registry.gitlab.com/amadoruavs/pidgin/common:nvidia
        sudo docker push registry.gitlab.com/amadoruavs/pidgin/common:mesa
    fi
fi
