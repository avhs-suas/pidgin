# Modified from original interop-client image.
FROM auvsisuas/interop-client

# Uninstall python2 so we don't have issues
RUN apt remove -y python

# Get important dev tools
RUN apt update && apt upgrade -y
RUN DEBIAN_FRONTEND=noninteractive apt install -y keyboard-configuration
RUN DEBIAN_FRONTEND=noninteractive apt install -y tzdata
RUN ln -sf /usr/share/zoneinfo/America/New_York /etc/localtime
RUN apt install -y cmake git build-essential vim nmap

# Install interop library globally for convenience (venv is in /interop/client)
RUN pip3 install -r requirements.txt
RUN python3 setup.py install

# Install MAVSDK Python Library (separate from the C++ library built below)
RUN pip3 install mavsdk

# Add suasdev user
RUN useradd -ms /bin/bash suasdev -p $(echo "AVuav2018" | openssl passwd -1 -stdin)
RUN usermod -a -G sudo suasdev

USER suasdev
WORKDIR /home/suasdev
RUN mkdir pidgin

# Build MAVSDK
RUN git clone https://github.com/mavlink/mavsdk --recursive
WORKDIR mavsdk
RUN git submodule update --init --recursive
RUN cmake -DCMAKE_BUILD_TYPE=Debug -DBUILD_SHARED_LIBS=ON -Bbuild/default -H.
RUN cmake --build build/default

# Install MAVSDK system wide
USER root
RUN cmake --build build/default --target install
RUN ldconfig
USER suasdev
