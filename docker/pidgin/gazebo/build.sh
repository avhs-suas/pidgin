# Builds, tags and (optionally) pushes gazebo images

# Automatically exit on any command failure
set -e

# idiomatic parameter and option handling in sh
# Copied from SuperUser/StackExchange
TOBUILD="DEFAULT"
PUSH="FALSE"

# Define the help menu manually because apparently you need to in bash
help() {
    echo "Usage: ./build.sh [--all] [--push] [--help]"
    echo
    echo "  --all       Build both Docker images (not recommended for general use)"
    echo "  --push      Push images to registry once built"
    echo "  --help      Print this help"
}

while test $# -gt 0
do
    case "$1" in
        --all) echo "Building all Docker common images."
               TOBUILD="ALL"
            ;;
        --push) echo "Pushing Docker images to registry when done."
               PUSH="TRUE"
            ;;
        --help) help; exit 0
            ;;
        *) echo "WARN: Unrecognized argument $1, ignoring"
            ;;
    esac
    shift
done

# Check if we're building ALL; if not, autodetect driver

if [ "$TOBUILD" != "ALL" ]; then
    # Check whether we're using NVIDIA
    if command -v nvidia-smi >/dev/null 2>&1; then
        # We're using an nvidia driver
        echo "Using NVIDIA proprietary driver"
        sudo docker build --no-cache -t registry.gitlab.com/amadoruavs/pidgin/gazebo:nvidia nvidia/

        if [ "$PUSH" == "TRUE" ]; then
            # Push mesa image
            sudo docker push registry.gitlab.com/amadoruavs/pidgin/gazebo:nvidia
        fi

    else
        echo "Using open source MESA driver"
        # We're not using nvidia, it's probably safe to use MESA
        sudo docker build --no-cache -t registry.gitlab.com/amadoruavs/pidgin/gazebo:mesa mesa/

        if [ "$PUSH" == "TRUE" ]; then
            # Push mesa image
            sudo docker push registry.gitlab.com/amadoruavs/pidgin/gazebo:mesa
        fi
    fi
else
    # Build all
    # Use automagic to get NVIDIA driver version and download right version
    sudo docker build --no-cache -t registry.gitlab.com/amadoruavs/pidgin/gazebo:nvidia nvidia
    sudo docker build --no-cache -t registry.gitlab.com/amadoruavs/pidgin/gazebo:mesa mesa

    if [ "$PUSH" == "TRUE" ]; then
        # Push all
        sudo docker push registry.gitlab.com/amadoruavs/pidgin/gazebo:nvidia
        sudo docker push registry.gitlab.com/amadoruavs/pidgin/gazebo:mesa
    fi
fi
